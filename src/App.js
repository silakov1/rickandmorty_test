import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';

import Layout from './Pages/layout';
import Home from './Pages/Home';
import Character from './Pages/Character';
import ErrorPage from './Pages/ErrorPage';
import Contacts from './Pages/Contacts';
import About from './Pages/About';

const App = () => (
  <div className="App">
    <Provider store={store}>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/character/:id' component={Character} />
            <Route path='/contacts' component={Contacts} />
            <Route path='/about' component={About} />
            <Route path='*' component={ErrorPage} />
          </Switch>
        </Layout>
      </BrowserRouter>
    </Provider>
  </div>
)

export default App;
