import React, { Fragment } from 'react';
import styles from "./layout.module.css";

const About = () => (
  <Fragment>
    <h1>About</h1>
    <div className={styles.content}>
      <p>На основе API <a href="https://rickandmortyapi.com/">https://rickandmortyapi.com/</a>, реализовать React приложение, lazy-список героев из мультсериала (lazy-список - список, в котором элементы подгружаются с бекенда частями, т.е. когда скролл доходит до конца страницы - получаем следующую часть списка и т.д. пока у бекенда еще есть для нас данные).</p>
      <ul>
        <li>По нажатию на персонажа должен осуществляться переход на отдельную страницу с подробной информацией о нём.</li>
        <li>Для управлением стейта использовать Redux, для асинхронных запросов Redux-Saga. </li>
        <li>Для стилей использовать SCSS или StyledComponents.</li>
        <li>Тесты приветствуются.</li>
      </ul>
    </div>
  </Fragment>
)

export default About
